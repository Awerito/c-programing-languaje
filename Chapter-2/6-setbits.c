#include <stdio.h>

unsigned getbits(unsigned, int, int);
unsigned setbits(unsigned, int, int, unsigned);

int main(int argc, char *argv)
{
    printf("%u\n", setbits(5, 0, 1, 5));
    return 1;
}

unsigned getbits(unsigned x, int p, int n)
{
    return (x >> (p + 1 - n)) & ~(~0 << n);
}

unsigned setbits(unsigned x, int p, int n, unsigned y)
{
    return (x << n) | getbits(y, p, n);
}
