#include <stdio.h>

int any(char s1[], char s2[]);

int main(int argc, char *argv[])
{
    if (argc > 2) {
        int index = any(argv[1], argv[2]);
        printf("%d\n", index);
    } else printf("Not enough args\n");

    return 0;
}

int any(char s1[], char s2[])
{
    int i = 0, j = 0, occ;

    while (s1[i] != '\0') {

        while (s1[i] != s2[0] && s1[i] != '\0')
            i++;

        if (s1[i] == '\0')
            return -1;

        occ = i;

        while (s1[i] == s2[j] && s1[i] != '\0' && s2[j] != '\0') {
            i++;
            j++;
        }

        if (s2[j] == '\0')
            return occ;

        if (s1[i] == '\0')
            return -1;

        i = occ + 1;
        j = 0;
    }
}
