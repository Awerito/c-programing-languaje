#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int htoi(char s[]);

int main(int argc, char *argv[])
{
    int result = -1;

    if (argc > 1)
        result = htoi(argv[1]);

    printf("%d\n", result);

    return 0;
}

int htoi(char s[])
{
    int result = 0, i = 0;

    if (strlen(s) >= 2)
        if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X'))
            i = 2;

    char c;
    for (i; (c = s[i]) != '\0'; i++) {
        int value;
        if ('0' <= c && c <= '9')
            value = c - '0';
        else if ('a' <= c && c <= 'f')
            value = 10 + c - 'a';
        else if ('A' <= c && c <= 'F')
            value = 10 + c - 'A';
        else
            return -1;

        result = result * 16 + value;
    }

    return result;
}
