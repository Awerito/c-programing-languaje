#include <stdio.h>
#include <string.h>
#define SIZE 1000

void squeeze(char s1[], char s2[]);

int main(int argc, char *argv[])
{
    if (argc > 2) {
        squeeze(argv[1], argv[2]);
        printf("%s\n", argv[1]);
    } else {
        printf("Not enough args \n");
    }

    return 0;
}

void squeeze(char s1[], char s2[])
{
    int j = 0;
    for (int i = 0; s1[i] != '\0'; i++)
        if (s1[i] != s2[i])
            s1[j++] = s1[i];
    s1[j] = '\0';
}
