#include <stdio.h>

int bitcount(unsigned);

int main(int argc, char *argv[])
{
    printf("%d\n", bitcount(10));

    return 0;
}

int bitcount(unsigned n)
{
    int i = 0;

    for (i; n != 0; n &= n - 1)
        i++;

    return i;
}
