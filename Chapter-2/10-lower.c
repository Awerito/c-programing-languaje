#include <stdio.h>
#include <string.h>

int lower(char);

int main(int argc, char *argv[])
{
    if (argc > 1)
        for (int i = 0; i < strlen(argv[1]); i++)
            putchar(lower(argv[1][i]));

    return 0;
}

int lower(char c)
{
    return (c >= 'A' && c <= 'Z') ? c + 'a' - 'A': c;
}
