#include <stdio.h>

int main(void)
{
    int c, lim = 10;
    for (int i = 0; (i < lim - 1) == ((c = getchar()) != '\n') == (c != EOF); i++) {
        continue;
    }

    return 0;
}
