#include <stdio.h>

unsigned rightrot(unsigned x, int n);

int main(void)
{
    printf("%u\n", rightrot(5732, 3));
    return 0;
}

unsigned rightrot(unsigned x, int n)
{   
    while (n-- > 0)
        if(x & 1)
            x = (x >> 1) | ~(~0U >> 1);
        else
            x = x >> 1;
    return x;
}
