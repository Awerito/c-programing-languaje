#include <stdio.h>

unsigned getbits(unsigned, int, int);
unsigned invert(unsigned, int, int);

int main(int argc, char *argv)
{
    printf("%u\n", invert(5, 0, 1));
    return 1;
}

unsigned getbits(unsigned x, int p, int n)
{
    return (x >> (p + 1 - n)) & ~(~0 << n);
}

unsigned invert(unsigned x, int p, int n)
{
    return  (x & ~(~0 << n)) & ~getbits(x, p, n);
}
