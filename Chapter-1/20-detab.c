#include <stdio.h>
#define MAXLINE 1000

int tabs = 4;

/*
 * replace tabs by tabs spaces
 */
int main()
{
    int c, i = 0;
    char text[MAXLINE];

    while ((c = getchar()) != EOF) {
        if (c == '\t')
            for (int j = 0; j < tabs; ++j) {
                text[i] = ' ';
                ++i;
            }
        else {
            text[i] = c;
            ++i;
        }
    }

    printf("%s", text);

    return 0;
}
