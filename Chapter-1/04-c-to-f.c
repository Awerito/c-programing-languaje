#include <stdio.h>
#define LOWER 0
#define UPPER 300
#define STEP 20


/*
 * print Celsius-Fahrenheit table for celsius = 0, 20, ..., 300
 */
int main()
{
    printf("Celsius\tFahrenheit\n");
    for(float celsius = LOWER; celsius <= UPPER; celsius += STEP)
        printf("%3.0f\t\t%6.1f\n", celsius, (5.0 / 9.0) * (celsius - 32.0));

    return 0;
}
