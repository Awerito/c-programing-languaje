#include <stdio.h>
#define MAXLINE 1000
#define TABS 4

/*
 * replace tabs by tabs spaces
 */
int main()
{
    int c, i = 0;
    int spaces = 0;
    char text[MAXLINE];

    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            spaces++;
            if (spaces == 4) {
                text[i] = '\t';
                i++;
                spaces = 0;
            }
        } else {
            while (spaces != 0) {
                text[i] = ' ';
                i++;
                spaces--;
            }
            text[i] = c;
            ++i;
        }
    }

    printf("%s", text);

    return 0;
}
