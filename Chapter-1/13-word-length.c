#include <stdio.h>
#include <stdbool.h>

/*
 * count digits, white space, others
 */
int main()
{
    int c;
    int word_size = 0;
    bool state = false;

    printf("Words length:");
    while((c = getchar()) != EOF) {
        if(c == ' ' || c == '\t' || c == '\n') {
            state = false;
            printf(" %d", word_size);
            word_size = 0;
        }
        else if(state == false) {
            state = true;
        }

        if (state == true)
            ++word_size;
    }

    printf("\n");

    return 0;
}
