#include <stdio.h>
#include <stdbool.h>

/*
 * remove excess blank space from string
 */
int main()
{
    int c;
    bool replace = true;

    while ((c = getchar()) != EOF) {
        switch(c) {
            case ' ':
            case '\t':
            case '\n':
                if (replace == false) {
                    putchar(' ');
                    replace = true;
                }
                break;
            default:
                putchar(c);
                replace = false;
                break;
        }
    }

    printf("\n");

    return 0;
}
