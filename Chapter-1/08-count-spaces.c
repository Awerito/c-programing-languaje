#include <stdio.h>

/*
 * count blank, tabs and break lines characters in input
 */
int main()
{
    int c;
    long nc = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\t' || c == '\n')
            ++nc; //nc++; is also valid in this context
    }
    printf("%ld\n", nc);

    return 0;
}
