#include <stdio.h>
#define MAXLINE 1000

int getline_length(char line[], int maxline);
void copy(char to[], char from[]);

/* print longest input line */
int main()
{
    int len;
    char line[MAXLINE];

    while ((len = getline_length(line, MAXLINE)) > 0) {
        for (int i = len; i >= 0; i--)
            printf("%c", line[i]);
        printf("\n");
    }

    return 0;
}

/* getline_length: read a line into s, return length */
int getline_length(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;

    if (c == '\n') {
        s[i] = c;
        ++i;
    }

    s[i] = '\0';
    return i;
}
