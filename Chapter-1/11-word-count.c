#include <stdio.h>

#define IN 1
#define OUT 0

/*
 * count lines, words, and characters in input
 */
int main()
{
    int c, word_count, char_count, state;

    state = OUT;
    word_count = char_count = 0;
    while((c = getchar()) != EOF) {
        ++char_count;
        if(c == ' ' || c == '\t')
            state = OUT;
        else if(state == OUT) {
            state = IN;
            ++word_count;
        }
    }

    printf("Chars: %d Words: %d\n", char_count, word_count);

    return 0;
}
