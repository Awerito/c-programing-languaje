#include <stdio.h>
#define LOWER 0
#define UPPER 300
#define STEP 20

/*
 * print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300
 */
int main()
{
    printf("Fahrenheit\tCelsius\n");
    for(float fahr = LOWER; fahr <= UPPER; fahr += STEP)
        printf("%3.f\t\t%6.f\n", fahr, (9.0 / 5.0) * fahr + 32.0);

    return 0;
}
