#include <stdio.h>
#define MAXCOL 40
#define NO_BLANK -1

int last_blank(char arr[], int len);

int main()
{
    int cursor, col = 0;
    int c, i, j, last_blank_col;
    char line[MAXCOL + 1];
    
    while ((c = getchar()) != EOF) {
        line[cursor++] = c;
        col++;

        if (col >= MAXCOL || c == '\n') {
            line[cursor] = '\0';
            
            if ((last_blank_col = last_blank(line, cursor)) == NO_BLANK) {
                for (i = 0; i < cursor; ++i)
                    putchar(line[i]);
                col = cursor = 0;
            } else {
                for (i = 0; i < last_blank_col; ++i)
                    putchar(line[i]);

                for (i = 0, j = last_blank_col + 1, col = 0; j < cursor; ++i, ++j) {
                    line[i] = line[j];
                    col++;
                }

                cursor = i;
            }

            putchar('\n');
        }
    }
    
    return 0;
}

int last_blank(char arr[], int len)
{
    int i, last_blank_col;
    
    last_blank_col = -1;
    for (i = 0; i < len; ++i)
        if (arr[i] == ' ' || arr[i] == '\t' || arr[i] == '\n')
            last_blank_col = i;
    
    return last_blank_col;
}
