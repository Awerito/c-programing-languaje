#include <stdio.h>
#define LOWER 0
#define UPPER 300
#define STEP 20

float f_to_c(float fahr);
float c_to_f(float celsius);

/*
 * print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300
 */
int main()
{
    printf("Fahrenheit\tCelsius\n");
    for(float fahr = LOWER; fahr <= UPPER; fahr += STEP)
        printf("%3.f\t\t%6.f\n", fahr, f_to_c());

    return 0;
}

/*
 * Fahrenheit to Celsius converter
 */
float f_to_c(float fahr)
{
    return (9.0 / 5.0) * fahr + 32.0;
}

/*
 * Fahrenheit to Celsius converter
 */
float c_to_f(float celsius)
{
    return (5.0 / 9.0) * (fahr - 32.0);
}
