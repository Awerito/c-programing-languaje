#include <stdio.h>
#define MAXLINE 10000

/* print longest input line */
int main()
{
    int c, i = 0;
    char min_text[MAXLINE];

    while ((c = getchar()) != EOF)
        if (c != ' ' && c != '\t' && c != '\n') {
            min_text[i] = c;
            i++;
        }

    printf("%s\n", min_text);

    return 0;
}
