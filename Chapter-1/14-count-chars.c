#include <stdio.h>
#define ASCII_TABLE_SIZE 128

// z
int main()
{
    int c;
    int ascii_table[ASCII_TABLE_SIZE];

    for (int i = 0; i < ASCII_TABLE_SIZE; i++)
        ascii_table[i] = 0;

    while ((c = getchar()) != EOF)
        ++ascii_table[c];

    for (int i = 9; i < ASCII_TABLE_SIZE; i++) {
        if (ascii_table[i] > 0) {
            if (i == ' ')
                printf("\\b");
            else if (i == '\t')
                printf("\\t");
            else if (i == '\n')
                printf("\\n");
            else
                putchar(i);

            printf("\t");
            for (int j = 0; j < ascii_table[i]; j++)
                printf("+");

            printf("\n");
        }
    }

    return 0;
}
