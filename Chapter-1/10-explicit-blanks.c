#include <stdio.h>
#include <stdbool.h>

/*
 * print explicit blank chars
 */
int main()
{
    int c;

    while((c = getchar()) != EOF) {
        switch(c) {
            case ' ':
                putchar('\\');
                putchar('b');
                break;
            case '\t':
                putchar('\\');
                putchar('t');
                break;
            case '\n':
                putchar('\\');
                putchar('n');
                break;
            case '\\':
                putchar('\\');
                putchar('\\');
                break;
            default:
                putchar(c);
                break;
        }
    }

    printf("\n");

    return 0;
}
