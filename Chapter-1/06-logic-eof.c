#include <stdio.h>

/*
 * print getchar() != EOF
 */
int main()
{
    printf("%d\n", getchar() != EOF);

    return 0;
}
