#include <stdio.h>
#define MAXLINE 1000
#define LINE_LENGTH 50

int getline_length(char line[], int maxline);
void copy(char to[], char from[]);

/* print longest input line */
int main()
{
    int len;
    char line[MAXLINE], longest[MAXLINE];

    while ((len = getline_length(line, MAXLINE)) > 0)
        if (len >= LINE_LENGTH) {
            copy(longest, line);
            printf("Line length: %d\n", len);
            printf("%s\n", longest);
        }

    return 0;
}

/* getline_length: read a line into s, return length */
int getline_length(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;

    if (c == '\n') {
        s[i] = c;
        ++i;
    }

    s[i] = '\0';
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i = 0;

    while ((to[i] = from[i]) != '\0')
        ++i;
}
