#include <stdio.h>

#define IN 1
#define OUT 0

/*
 * print a text one word per line
 */
int main()
{
    int c, state;

    state = OUT;
    while((c = getchar()) != '\n') {
        if(c == ' ' || c == '\t'){
            if(state == OUT) {
                putchar('\n');
                state = IN;
            }
        } else {
            putchar(c);
            state = OUT;
        }
    }
    putchar('\n');

    return 0;
}
